package builder;

public interface ProjectPlan {
	public void setProjectName(String projectname);
	public void setProjectEmail(String email);
	public void setProjectSupervisor(String supervisor);
	public void setProjectDescription(String description);
	public void setProjectDept(String dept);
	public void setProjectGroup(String group);
}
