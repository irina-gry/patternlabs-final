package configreader;

import configreader.build.ConcreteConfigReader;
import configreader.build.XMLConfigAPIFactory;
import configreader.build.YAMLConfigAPIFactory;
import configreader.xml.XMLBuildConfigurationReader;
import configreader.yaml.YamlBuildConfigurationReader;

public class ConfigurationReader {

	
	
	public static void main(String[] args) {
	/*	XMLBuildConfigurationReader buildConfigurationReader = new XMLBuildConfigurationReader("build.xml");
		System.out.println("XML Build config: "+buildConfigurationReader.getProject());
		YamlBuildConfigurationReader yamlConfigReader = new YamlBuildConfigurationReader("build.yaml");
		System.out.println("Yaml build config: "+yamlConfigReader.getBuildConfig());
	*/
		//  � �������������� concrete reader
		
		new ConcreteConfigReader("build.xml",new XMLConfigAPIFactory().getAPI()).getConfig();
		new ConcreteConfigReader("build.yaml",new YAMLConfigAPIFactory().getAPI()).getConfig();
	}
}
