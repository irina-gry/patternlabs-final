package configreader.build;

import configreader.xml.XMLBuildConfigurationReader;
import configreader.build.Config;
import configreader.build.ProjectAdapter;
import configreader.build.ConfigReaderAPI;

public class XMLConfigAPI implements ConfigReaderAPI {
	  public XMLConfigAPI() {
	    }
	  @Override
		public Config getConfig(String filename) {
			// TODO Auto-generated method stub
			Config config = new ProjectAdapter(new XMLBuildConfigurationReader(filename).getProject());
			System.out.println("XML Build config: "+ config);
			return config;
		}
}
