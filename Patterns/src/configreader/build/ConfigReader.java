package configreader.build;

import configreader.build.Config;
import configreader.build.ConfigReaderAPI;

public abstract class ConfigReader {

    public ConfigReaderAPI configReaderAPI;

    public ConfigReader(ConfigReaderAPI configReaderAPI) {
    	this.configReaderAPI = configReaderAPI;
    }

    public abstract Config getConfig();
}