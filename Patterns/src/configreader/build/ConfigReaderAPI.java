package configreader.build;


public interface ConfigReaderAPI {
	public Config getConfig(String filename);
}
