package configreader.build;

import java.util.List;

import configreader.domain.Project;
import configreader.domain.Property;
import configreader.domain.Target;
import configreader.domain.BuildConfig;

public class BuildConfigAdapter implements Config {
	
	private BuildConfig buildConfig;
	public BuildConfigAdapter(BuildConfig buildConfig){
		this.buildConfig = buildConfig;
	}
	
	@Override
	public Project getProject() {
		// TODO Auto-generated method stub
		return this.buildConfig.getProject();
	}

	@Override
	public void setProject(Project project) {
		// TODO Auto-generated method stub
		this.buildConfig.setProject(project);
	}
	@Override
	public String toString() {
		return buildConfig.toString();
	}
	@Override
	public String getDescription() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void setDescription(String description) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public String getSrc() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void setSrc(String src) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public String getBuild() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void setBuild(String build) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public String getDist() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void setDist(String dist) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public List<Target> getTargets() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void setTargets(List<Target> targets) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public String getName() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void setName(String name) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public Target getDefaultTarget() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void setDefaultTarget(Target defaultTarget) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public String getBasedir() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void setBasedir(String basedir) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public List<Property> getProperties() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void setProperties(List<Property> properties) {
		// TODO Auto-generated method stub
		
	}

}
