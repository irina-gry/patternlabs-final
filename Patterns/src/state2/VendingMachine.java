package state2;

public class VendingMachine {
 
	State soldOutState;
	State noCoinState;
	State hasCoinState;
	State soldState;
	State brokenState;
 
	State state = soldOutState;
	int count = 0;
 
	public VendingMachine(int count) {
		soldOutState = new SoldOutState(this);
		noCoinState = new NoCoinState(this);
		hasCoinState = new HasCoinState(this);
		soldState = new SoldState(this);
		brokenState = new BrokenState(this);

		this.count = count;
 		if (count > 0) {
			state = noCoinState;
		} 
	}
 
	public void insertCoin() {
		state.insertCoin();
	}
 
	public void ejectCoin() {
		state.ejectCoin();
	}
 
	public void pushSelectButton() {
		state.pushSelectButton();
		state.dispense();
	}

	void setState(State state) {
		this.state = state;
	}
 
	void dispense() {
		System.out.println("����� ��������...");
		if (count != 0) {
			count = count - 1;
		}
	}
 
	int getCount() {
		return count;
	}
 
	void refill(int count) {
		this.count = count;
		state = noCoinState;
	}

    public State getState() {
        return state;
    }

    public State getSoldOutState() {
        return soldOutState;
    }

    public State getNoCoinState() {
        return noCoinState;
    }

    public State getHasCoinState() {
        return hasCoinState;
    }

    public State getSoldState() {
        return soldState;
    }
 
	public String toString() {
		StringBuffer result = new StringBuffer();
		result.append("\n�������� �������");
		result.append("\n� �������: " + count + " �������");
		result.append("\n���������: ");
		if (count != 1) {
			result.append(state);
		}
		result.append("\n");
		return result.toString();
	}
}
