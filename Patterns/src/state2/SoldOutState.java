package state2;

public class SoldOutState implements State {
    VendingMachine vendingMachine;
 
    public SoldOutState(VendingMachine vendingMachine) {
        this.vendingMachine = vendingMachine;
    }
 
	public void insertCoin() {
		System.out.println("������ ��������� ������, ��� ������ ����������");
	}
 
	public void ejectCoin() {
		System.out.println("���������� �������, ������ ��� �� ��������");
	}
 
	public void pushSelectButton() {
		System.out.println("��������� ������ �����������");
	}
 
	public void dispense() {
		System.out.println("����� �� �����");
	}
 
	public String toString() {
		return "������ ����������";
	}
}
