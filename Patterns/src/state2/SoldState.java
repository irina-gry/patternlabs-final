package state2;

public class SoldState implements State {
 
    VendingMachine vendingMachine;
 
    public SoldState(VendingMachine vendingMachine) {
        this.vendingMachine = vendingMachine;
    }
       
	public void insertCoin() {
		System.out.println("���������, ����� ��������");
	}
 
	public void ejectCoin() {
		System.out.println("�� ��� ������� �����, ������ ����������");
	}
 
	public void pushSelectButton() {
		System.out.println("������� �������");
	}
 
	public void dispense() {
		vendingMachine.dispense();
		if (vendingMachine.getCount() > 0) {
			vendingMachine.setState(vendingMachine.getNoCoinState());
		} else {
			System.out.println("������ �����������!");
			vendingMachine.setState(vendingMachine.getSoldOutState());
		}
	}
 
	public String toString() {
		return "����� ��������";
	}
}


