package cars;

import java.util.Scanner;
import java.util.ArrayList;


public class CarDealership {
	private ArrayList<SalableVehicle> carLot;  
	private String name; 

	public CarDealership() {
		carLot = new ArrayList<SalableVehicle>();
		name = null;
	}

	public CarDealership(String name) {
		carLot = new ArrayList<SalableVehicle>();
		this.name = name;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public void addCar(SalableVehicle c) {
		carLot.add(c);
	}

	public void showCars() {
		System.out.println("��������� " + name + ", � �������:");
		for (SalableVehicle c : carLot) {
			System.out.println(c);
		}
	}

        public SalableVehicle findCar(String vin) throws NoSuchCarException {
 	    for (SalableVehicle c : carLot) {
	        if (c.getVin().equals(vin)) {
                    return c;
                }
	    }
            throw new NoSuchCarException("������: ��� ���������� � ����� VIN");
        }

        public void sellCar(String vin) throws NoSuchCarException {
 	    for (SalableVehicle c : carLot) {
	        if (c.getVin().equals(vin)) {
                    carLot.remove(c);
                    return;
                }
	    }
 	    	throw new NoSuchCarException("������: ��� ���������� � ����� VIN");
        }

	public static void main(String argv[]) throws NoSuchCarException {
		CarDealership cd = new CarDealership();
		cd.setName("������ ���������");
		SalableVehicle s = new SalableCar("8383", "FORD", "FIESTA", "�����", 2008, 50000, 240000);
		SalableVehicle s1 = new SalableCar("8384", "KIA", "RIO", "�����", 2006, 70000, 190000);
		cd.addCar(s);
		cd.addCar(s1);
		cd.showCars();
	}
}
