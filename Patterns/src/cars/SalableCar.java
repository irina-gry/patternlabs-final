package cars;

public class SalableCar extends SalableVehicle {
	
	Car car;

	 private String make; 
	 private String model; 
	 private String color;
	 private int year;
	 
	public SalableCar(String vin, String make, String model, String color, int year, int mileage, int price) {
	super(vin, mileage, price);
	car = new Car (make, model, color, year);
	this.vin=vin;
	this.make=make;
	this.model=model;
	this.color=color;
	this.year=year;
	this.mileage=mileage;
	this.price=price;
	}

	@Override
	public String getMakeAndModel() {
		
		return car.getMake()+" "+car.getModel();
	}

	@Override
	public String getColor() {
		return car.getColor();
	}

	@Override
	public int getYear() {
		return car.getYear();
	}

	@Override
	public String toString() {
		 return "����: " + "\n"+ vin 
				 + "\n �����: "+ make 
				 + "\n ������: "+ model 
				 + "\n ����: "+ color 
				 + "\n ���: "+ year
				 + "\n ������: "+ mileage 
				 + "\n ����: "+ price 
                ;
	}

}
